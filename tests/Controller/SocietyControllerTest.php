<?php

namespace App\Test\Controller;

use App\Entity\Society;
use App\Repository\SocietyRepository;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class SocietyControllerTest extends WebTestCase
{
    private KernelBrowser $client;
    private SocietyRepository $repository;
    private string $path = '/society/';

    protected function setUp(): void
    {
        $this->client = static::createClient();
        $this->repository = static::getContainer()->get('doctrine')->getRepository(Society::class);

        foreach ($this->repository->findAll() as $object) {
            $this->repository->remove($object, true);
        }
    }

    public function testIndex(): void
    {
        $crawler = $this->client->request('GET', $this->path);

        self::assertResponseStatusCodeSame(200);
        self::assertPageTitleContains('Society index');

        // Use the $crawler to perform additional assertions e.g.
        // self::assertSame('Some text on the page', $crawler->filter('.p')->first());
    }

    public function testNew(): void
    {
        $originalNumObjectsInRepository = count($this->repository->findAll());

        $this->markTestIncomplete();
        $this->client->request('GET', sprintf('%snew', $this->path));

        self::assertResponseStatusCodeSame(200);

        $this->client->submitForm('Save', [
            'society[name]' => 'Testing',
            'society[sirenNumber]' => 'Testing',
            'society[cityImmatriculation]' => 'Testing',
            'society[dateImmatriculation]' => 'Testing',
            'society[capital]' => 'Testing',
            'society[legalForm]' => 'Testing',
        ]);

        self::assertResponseRedirects('/society/');

        self::assertSame($originalNumObjectsInRepository + 1, count($this->repository->findAll()));
    }

    public function testShow(): void
    {
        $this->markTestIncomplete();
        $fixture = new Society();
        $fixture->setName('My Title');
        $fixture->setSirenNumber('My Title');
        $fixture->setCityImmatriculation('My Title');
        $fixture->setDateImmatriculation('My Title');
        $fixture->setCapital('My Title');
        $fixture->setLegalForm('My Title');

        $this->repository->save($fixture, true);

        $this->client->request('GET', sprintf('%s%s', $this->path, $fixture->getId()));

        self::assertResponseStatusCodeSame(200);
        self::assertPageTitleContains('Society');

        // Use assertions to check that the properties are properly displayed.
    }

    public function testEdit(): void
    {
        $this->markTestIncomplete();
        $fixture = new Society();
        $fixture->setName('My Title');
        $fixture->setSirenNumber('My Title');
        $fixture->setCityImmatriculation('My Title');
        $fixture->setDateImmatriculation('My Title');
        $fixture->setCapital('My Title');
        $fixture->setLegalForm('My Title');

        $this->repository->save($fixture, true);

        $this->client->request('GET', sprintf('%s%s/edit', $this->path, $fixture->getId()));

        $this->client->submitForm('Update', [
            'society[name]' => 'Something New',
            'society[sirenNumber]' => 'Something New',
            'society[cityImmatriculation]' => 'Something New',
            'society[dateImmatriculation]' => 'Something New',
            'society[capital]' => 'Something New',
            'society[legalForm]' => 'Something New',
        ]);

        self::assertResponseRedirects('/society/');

        $fixture = $this->repository->findAll();

        self::assertSame('Something New', $fixture[0]->getName());
        self::assertSame('Something New', $fixture[0]->getSirenNumber());
        self::assertSame('Something New', $fixture[0]->getCityImmatriculation());
        self::assertSame('Something New', $fixture[0]->getDateImmatriculation());
        self::assertSame('Something New', $fixture[0]->getCapital());
        self::assertSame('Something New', $fixture[0]->getLegalForm());
    }

    public function testRemove(): void
    {
        $this->markTestIncomplete();

        $originalNumObjectsInRepository = count($this->repository->findAll());

        $fixture = new Society();
        $fixture->setName('My Title');
        $fixture->setSirenNumber('My Title');
        $fixture->setCityImmatriculation('My Title');
        $fixture->setDateImmatriculation('My Title');
        $fixture->setCapital('My Title');
        $fixture->setLegalForm('My Title');

        $this->repository->save($fixture, true);

        self::assertSame($originalNumObjectsInRepository + 1, count($this->repository->findAll()));

        $this->client->request('GET', sprintf('%s%s', $this->path, $fixture->getId()));
        $this->client->submitForm('Delete');

        self::assertSame($originalNumObjectsInRepository, count($this->repository->findAll()));
        self::assertResponseRedirects('/society/');
    }
}
