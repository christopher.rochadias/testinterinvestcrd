<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230119215532 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE archive_adress (archive_id INT NOT NULL, adress_id INT NOT NULL, PRIMARY KEY(archive_id, adress_id))');
        $this->addSql('CREATE INDEX IDX_A1E9D3AB2956195F ON archive_adress (archive_id)');
        $this->addSql('CREATE INDEX IDX_A1E9D3AB8486F9AC ON archive_adress (adress_id)');
        $this->addSql('ALTER TABLE archive_adress ADD CONSTRAINT FK_A1E9D3AB2956195F FOREIGN KEY (archive_id) REFERENCES archive (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE archive_adress ADD CONSTRAINT FK_A1E9D3AB8486F9AC FOREIGN KEY (adress_id) REFERENCES adress (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE archive_adress DROP CONSTRAINT FK_A1E9D3AB2956195F');
        $this->addSql('ALTER TABLE archive_adress DROP CONSTRAINT FK_A1E9D3AB8486F9AC');
        $this->addSql('DROP TABLE archive_adress');
    }
}
