<?php

namespace App\Form;

use App\Entity\Adress;
use App\Entity\Society;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SocietyType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('name')
            ->add('sirenNumber')
            ->add('cityImmatriculation')
            ->add('dateImmatriculation', DateTimeType::class)
            ->add('capital')
            ->add('legalForm')
            ->add('adresses', EntityType::class, [
            'class' => Adress::class,
            'choice_label' => 'completeLocation',
            'multiple' => true,
            'expanded' => true
        ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Society::class,
        ]);
    }
}
