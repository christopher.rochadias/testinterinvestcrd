<?php

namespace App\Form;

use App\Entity\Adress;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AdressType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        // Un enum aurait été utilisé avec PHP 8.1
        $builder
            ->add('number')
            ->add('streetType', ChoiceType::class, [
                'choices' => [
                    'Allée' => 'Walkway',
                    'Rue' => 'Street',
                    'Lieu-dit' => 'Locality'
                ]
            ])
            ->add('streetName')
            ->add('city')
            ->add('zipCode')
            ->add('society')
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Adress::class,
        ]);
    }
}
