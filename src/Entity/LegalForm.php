<?php

namespace App\Entity;

use App\Entity\Common\NamableTrait;
use App\Repository\LegalFormRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: LegalFormRepository::class)]
class LegalForm
{
    use NamableTrait;

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    private string $name;

    public function getId(): ?int
    {
        return $this->id;
    }
}
