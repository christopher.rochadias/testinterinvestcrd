<?php

namespace App\Entity;

use App\Entity\Common\NamableTrait;
use App\Repository\SocietyRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Mapping\ClassMetadata;

#[ORM\Entity(repositoryClass: SocietyRepository::class)]
class Society
{
    use NamableTrait;

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private int $id;

    #[ORM\Column(length: 255)]
    private string $name;

    #[ORM\Column(length: 255)]
    private string $sirenNumber;

    #[ORM\Column(length: 255)]
    private string $cityImmatriculation;

    #[ORM\Column(length: 255, type: "datetime")]
    private \DateTime $dateImmatriculation;

    #[ORM\Column(length: 255)]
    private string $capital;

    #[ORM\ManyToOne(targetEntity: LegalForm::class)]
    private $legalForm;

    #[ORM\OneToMany(targetEntity: Adress::class, mappedBy: 'society')]
    private Collection $adresses;

    public function __construct()
    {
        $this->adresses = new ArrayCollection();
        $this->LegalForm = new ArrayCollection();
    }

    public static function loadValidatorMetadata(ClassMetadata $metadata)
    {
        $metadata->addPropertyConstraint('dateImmatriculation', new Assert\Type([
            'type' => \DateTime::class
        ]));
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getSirenNumber(): ?string
    {
        return $this->sirenNumber;
    }

    public function setSirenNumber(string $sirenNumber): self
    {
        $this->sirenNumber = $sirenNumber;

        return $this;
    }

    public function getCityImmatriculation(): ?string
    {
        return $this->cityImmatriculation;
    }

    public function setCityImmatriculation(string $cityImmatriculation): self
    {
        $this->cityImmatriculation = $cityImmatriculation;

        return $this;
    }

    public function getDateImmatriculation(): ?\DateTime
    {
        return $this->dateImmatriculation;
    }

    public function setDateImmatriculation(\DateTime $dateImmatriculation): self
    {
        $this->dateImmatriculation = $dateImmatriculation;

        return $this;
    }

    public function getCapital(): ?string
    {
        return $this->capital;
    }

    public function setCapital(string $capital): self
    {
        $this->capital = $capital;

        return $this;
    }

    public function getLegalForm(): ?LegalForm
    {
        return $this->legalForm;
    }

    public function setLegalForm($legalForm): self
    {
        $this->legalForm = $legalForm;

        return $this;
    }

    public function getAdresses(): Collection
    {
        return $this->adresses;
    }

    public function setAdresses($adresses): self
    {
        $this->adresses = $adresses;

        return $this;
    }

    public function addAdress(Adress $adress): self
    {
        if (!$this->adresses->contains($adress)) {
            $this->adresses[] = $adress;
        }

        return $this;
    }

    public function removeAdress(Adress $adress): self
    {
        $this->adresses->removeElement($adress);

        return $this;
    }

    public function addLegalForm(Archive $legalForm): self
    {
        if (!$this->LegalForm->contains($legalForm)) {
            $this->LegalForm->add($legalForm);
            $legalForm->setSociety($this);
        }

        return $this;
    }

    public function removeLegalForm(Archive $legalForm): self
    {
        if ($this->LegalForm->removeElement($legalForm)) {
            // set the owning side to null (unless already changed)
            if ($legalForm->getSociety() === $this) {
                $legalForm->setSociety(null);
            }
        }

        return $this;
    }
}
