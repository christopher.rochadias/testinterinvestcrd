<?php

namespace App\DataFixtures;

use App\Entity\Society;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class SocietyFixtures extends Fixture implements DependentFixtureInterface
{
    public const SOCIETY_REFERENCE = 'main-society';

    public function load(ObjectManager $manager): void
    {
        $society = new Society();
        $society->addAdress($this->getReference(AdressFixtures::ADRESS_REFERENCE));
        $society->setCapital('30000');
        $society->setCityImmatriculation('F32');
        $society->setName('InterInvest');
        $society->setDateImmatriculation(new \DateTime());
        $society->setSirenNumber('05646505FR');
        $society->setLegalForm($this->getReference(LegalFormFixtures::LEGAL_FORMS));

        $manager->persist($society);

        $this->setReference(self::SOCIETY_REFERENCE, $society);

        $manager->flush();
    }

    public function getDependencies()
    {
        return [
            AdressFixtures::class,
            LegalFormFixtures::class
        ];
    }
}
