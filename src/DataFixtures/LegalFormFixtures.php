<?php

namespace App\DataFixtures;

use App\Entity\LegalForm;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class LegalFormFixtures extends Fixture
{
    public const LEGAL_FORMS = 'main-legal-forms';

    public function load(ObjectManager $manager): void
    {
        $csv = fopen('public/legalforms.csv', 'r');

        while (!feof($csv)) {
            $line = fgetcsv($csv);

            $legalForm = new LegalForm();
            $legalForm->setName($line[0]);

            $manager->persist($legalForm);

            $this->setReference(self::LEGAL_FORMS, $legalForm);
        }
        fclose($csv);

        $manager->flush();
    }
}
