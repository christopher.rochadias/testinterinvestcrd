<?php

namespace App\DataFixtures;

use App\Entity\Adress;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class AdressFixtures extends Fixture
{
    public const ADRESS_REFERENCE = 'main-society';

    public function load(ObjectManager $manager): void
    {
        $adress = new Adress();
        $adress->setCity('Paris');
        $adress->setNumber(1);
        $adress->setStreetName('Rue Du Bois');
        $adress->setStreetType('Voie');
        $adress->setZipCode(75020);

        $this->setReference(self::ADRESS_REFERENCE, $adress);

        $manager->persist($adress);

        $manager->flush();
    }
}
