<?php

namespace App\Controller;

use App\Entity\LegalForm;
use App\Form\LegalFormType;
use App\Repository\LegalFormRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/legal/form')]
class LegalFormController extends AbstractController
{
    #[Route('/', name: 'app_legal_form_index', methods: ['GET'])]
    public function index(LegalFormRepository $legalFormRepository): Response
    {
        return $this->render('legal_form/index.html.twig', [
            'legal_forms' => $legalFormRepository->findAll(),
        ]);
    }

    #[Route('/new', name: 'app_legal_form_new', methods: ['GET', 'POST'])]
    public function new(Request $request, LegalFormRepository $legalFormRepository): Response
    {
        $legalForm = new LegalForm();
        $form = $this->createForm(LegalFormType::class, $legalForm);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $legalFormRepository->save($legalForm, true);

            return $this->redirectToRoute('app_legal_form_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('legal_form/new.html.twig', [
            'legal_form' => $legalForm,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_legal_form_show', methods: ['GET'])]
    public function show(LegalForm $legalForm): Response
    {
        return $this->render('legal_form/show.html.twig', [
            'legal_form' => $legalForm,
        ]);
    }

    #[Route('/{id}/edit', name: 'app_legal_form_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, LegalForm $legalForm, LegalFormRepository $legalFormRepository): Response
    {
        $form = $this->createForm(LegalFormType::class, $legalForm);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $legalFormRepository->save($legalForm, true);

            return $this->redirectToRoute('app_legal_form_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('legal_form/edit.html.twig', [
            'legal_form' => $legalForm,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_legal_form_delete', methods: ['POST'])]
    public function delete(Request $request, LegalForm $legalForm, LegalFormRepository $legalFormRepository): Response
    {
        if ($this->isCsrfTokenValid('delete'.$legalForm->getId(), $request->request->get('_token'))) {
            $legalFormRepository->remove($legalForm, true);
        }

        return $this->redirectToRoute('app_legal_form_index', [], Response::HTTP_SEE_OTHER);
    }
}
